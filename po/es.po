# Spanish translation for network-manager-iodine.
# Copyright (C) 2012 network-manager-iodine's COPYRIGHT HOLDER
# This file is distributed under the same license as the network-manager-iodine package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Daniel Mustieles <daniel.mustieles@gmail.com>, 2012, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: network-manager-iodine master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=network-manager-iodine&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2013-11-03 21:20+0000\n"
"PO-Revision-Date: 2013-11-20 13:47+0100\n"
"Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>\n"
"Language-Team: Español <gnome-es-list@gnome.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Gtranslator 2.91.5\n"

#: ../auth-dialog/main.c:152
#, c-format
msgid "You need to authenticate to access the Virtual Private Network '%s'."
msgstr "Debe autenticarse para acceder a la red privada virtual «%s»."

#. Otherwise, we have no saved password, or the password flags indicated
#. * that the password should never be saved.
#.
#: ../auth-dialog/main.c:165 ../auth-dialog/main.c:188
msgid "Authenticate VPN"
msgstr "Autenticar VPN"

#: ../auth-dialog/main.c:168
msgid "Password:"
msgstr "Contraseña:"

#: ../auth-dialog/vpn-password-dialog.c:90
#: ../properties/nm-iodine-dialog.ui.h:7
msgid "_Password:"
msgstr "C_ontraseña:"

#: ../auth-dialog/vpn-password-dialog.c:187
msgid "_Cancel"
msgstr "_Cancelar"

#: ../auth-dialog/vpn-password-dialog.c:188
msgid "_Ok"
msgstr "_Aceptar"

#: ../auth-dialog/vpn-password-dialog.c:223
msgid "Sh_ow passwords"
msgstr "M_ostrar contraseñas"

#: ../auth-dialog/vpn-password-dialog.c:248
#| msgid "Show password"
msgid "dialog-password"
msgstr "diálogo de contraseña"

#: ../properties/nm-iodine.c:46
msgid "Iodine DNS Tunnel"
msgstr "Túnel DNS iodine"

#: ../properties/nm-iodine.c:47
msgid "Tunnel connections via DNS."
msgstr "Conexiones de túnel mediante DNS."

#: ../properties/nm-iodine-dialog.ui.h:1
msgid "Saved"
msgstr "Guardado"

#: ../properties/nm-iodine-dialog.ui.h:2
msgid "Always ask"
msgstr "Preguntar siempre"

#: ../properties/nm-iodine-dialog.ui.h:3
msgid "General"
msgstr "General"

#: ../properties/nm-iodine-dialog.ui.h:4
msgid "_Toplevel Domain:"
msgstr "_Reino de nivel superior:"

#: ../properties/nm-iodine-dialog.ui.h:5
msgid "Optional"
msgstr "Opcional"

#: ../properties/nm-iodine-dialog.ui.h:6
msgid "_Nameserver:"
msgstr "_Servidor de nombres:"

#: ../properties/nm-iodine-dialog.ui.h:8
msgid "_Fragment Size:"
msgstr "Tamaño del _fragmento:"

#: ../properties/nm-iodine-dialog.ui.h:9
msgid "Show password"
msgstr "Mostrar contraseña"

#: ../src/nm-iodine-service.c:131
#, c-format
msgid "invalid integer property '%s' or out of range [%d -> %d]"
msgstr "propiedad «%s» entera no válida o fuera de rango [%d -> %d]"

#: ../src/nm-iodine-service.c:142
#, c-format
msgid "invalid boolean property '%s' (not yes or no)"
msgstr "propiedad «%s» booleana no válida (no es «sí» o «no»)"

#: ../src/nm-iodine-service.c:149
#, c-format
msgid "unhandled property '%s' type %s"
msgstr "propiedad «%s» de tipo %s sin manejar"

#: ../src/nm-iodine-service.c:163
#, c-format
msgid "property '%s' invalid or not supported"
msgstr "propiedad «%s» no válida o no soportada"

#: ../src/nm-iodine-service.c:179
msgid "No VPN configuration options."
msgstr "No hay opciones de configuración de VPN."

#: ../src/nm-iodine-service.c:198
msgid "No VPN secrets!"
msgstr "No hay secretos VPN."

#: ../src/nm-iodine-service.c:483
msgid "Could not find iodine binary."
msgstr "No se pudo encontrar el binario de iodine."
